#!/usr/bin/env python3

__title__    = 'sample googler plugin for go-peerflix'
__credits__  = ['i@zhimingwang.org', 'dsjkvf@gmail.com']


import argparse
import contextlib
import re
import sys
import os
import urllib.request
import subprocess
from subprocess import Popen, PIPE


@contextlib.contextmanager
def redirect_stdin(fp):
    saved_stdin = sys.stdin
    sys.stdin = fp
    yield
    sys.stdin = saved_stdin


def get_info(url):
    try:
        page = urllib.request.urlopen(url).read().decode('cp1251')
        tit = page[page.find('<title>') + 7 : page.find('</title>')]
        siz = re.findall('<li.*?[GM]B.*?li>', page)
        siz = re.sub(r'.*?([0-9.]*)&nbsp;([GM]B).*', r'\1 \2', siz[0])
        mag = re.findall('magnet:\?xt.*?"', page)[0][:-1]
        return [tit, siz, mag]
    except Exception:
        return ['', '', '']


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("urls", nargs="+")
    args = parser.parse_args()
    with open("/dev/tty", "r") as tty:
        with redirect_stdin(tty):
            for url in args.urls:
                t, s, m = get_info(url)
                if t is not '' and s is not '' and m is not '':
                    print("\033[1;37m" + "Title:" + "\033[0m")
                    print("\033[1;32m" + "  " + t[0 : t.find('::')] + "\033[0m")
                    print("\033[1;37m" + "Expected download size:" + "\033[0m")
                    print("\033[1;36m" + "  " + s + "\033[0m")
                    yn = input("\033[1;37m" + "Proceed? [Y/n] " + "\033[0m").strip().lower()
                    if not yn.startswith("n") and not yn.startswith("q"):
                        try:
                            if os.environ.get('TMUX') is not None:
                                subprocess.call(['tmux', 'split', 'peerflix', 'mpv', m])
                            else:
                                subprocess.call(['peerflix', 'mpv', m])
                        except KeyboardInterrupt:
                            print("** Exited **")
                else:
                    print("\033[1;41m" + "WARNING: No data found, try another result" + "\033[0m")


if __name__ == "__main__":
    main()
