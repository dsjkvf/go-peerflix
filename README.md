Go Peerflix
===========


## About

This is a fork of [`go-peerflix`](https://github.com/Sioro-Neoku/go-peerflix) repository, which in its turn is a Golang port of [peerflix](https://github.com/mafintosh/peerflix) project. All the credits naturally go to the original author, [Sioro Neoku](https://github.com/Sioro-Neoku/), and I strongly encourage you to use [the original version]().

This fork, however, contains several patches (which may be found in the [`patches`](https://bitbucket.org/dsjkvf/go-peerflix/src/master/patches/) folder and can be applied separately if needed), such as:

  - the location of files being downloaded changed from `$TMPDIR` to `$TMPDIR/go-peerflix`
  - if [the primary blocklist](http://john.bitsurge.net/public/biglist.p2p.gz) is broken or not accessible, [the alternate blocklist](https://github.com/sahsu/transmission-blocklist) will be downloaded

## Usage

After launching `go-peerflix`, the stream is accessible at [http://localhost:8080/](http://localhost:8080/)

    go-peerflix [magnet url|torrent path|torrent url]

To start playing in VLC:

    go-peerflix -player vlc [magnet url|torrent path|torrent url]

Currently supported players are: VLC, MPlayer and MPV

## Extras

Some exemplary scripts can be found in `extras` folder:

  - `peerflix`: a simple wrapper around `go-peerflix` that makes sure `go-peerflix` will run correctly in some corner cases and also will clear temp files on break;
  - `peerflix.py`: a Python script that will use [`googler`](https://github.com/jarun/googler) for traversing [http://rutracker.org](http://rutracker.org), and then will pass the recieved results to `go-peerflix`, like this:


    googler --url-handler=peerflix.py -w rutracker.org <SEARCH STRING>

  - `peerflix-front` and `peerflix-back`: shell scripts for the same task, should be used in a `tmux` session for a better experience, like this:


    googler --url-handler=peerflix-front -w rutracker.org <SEARCH STRING>

## Download

Binaries are available from the [Downloads](https://bitbucket.org/dsjkvf/go-peerflix/downloads/) page.

## Install

Or, if you have Golang environment configured, you may want to install it with the following command:

    go get bitbuxket.com/dsjkvf/go-peerflix

## Build

Or, if you are willing to build it yourself, just run this:

    go build .

Or, if [goxc](https://github.com/laher/goxc) is available, simultaneous builds for Linux, macOS and Windows can be produced by simply launching it:

    goxc

## License
[MIT](https://bitbucket.org/dsjkvf/go-peerflix/src/master/LICENSE)
